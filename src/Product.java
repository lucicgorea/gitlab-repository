public class Product {

    private String typeOfProduct;
    private String productName;
    private String createdBy;
    private String location;

    public String getCreatedBy() { return createdBy; }
    public String getLocation() { return location; }
    public String getProductName() { return productName; }
    public String getTypeOfProduct() {  return typeOfProduct; }

    public void setCreatedBy(String createdBy) { this.createdBy = createdBy; }
    public void setLocation(String location) { this.location = location;   }
    public void setProductName(String productName) { this.productName = productName; }
    public void setTypeOfProduct(String typeOfProduct) { this.typeOfProduct = typeOfProduct; }

}
