import java.util.Scanner;

public class Ex5 {
    public static void main(String[] args) {
        Scanner abc = new Scanner(System.in);
        System.out.println("Ex. 5:\n Introdu 3 numere: ");
        int a = abc.nextInt(), b = abc.nextInt(), c = abc.nextInt();

        if (a == b && b == c)
            System.out.println("Toate numerele sunt egale");
        else if(a != b && b != c && c != a)
            System.out.println("Toate numerele sunt diferite");

        else System.out.println("Nu sunt numere egale sau diferite");
    }
}
