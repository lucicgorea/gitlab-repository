public class TransportCompany {

    private int carIndex;
    private String trunk;

    public int getCarIndex() { return carIndex; }
    public String getTrunk() { return trunk; }

    public void setCarIndex(int carIndex) { this.carIndex = carIndex; }
    public void setTrunk(String trunk) { this.trunk = trunk; }

    public void transportation(String startLocation, String destination, String product){
        trunk = product;
        startLocation = destination;
        System.out.println(product+" transportet to "+destination);
    }

}
