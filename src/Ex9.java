import java.util.Scanner;


public class Ex9 {
    public static void main(String[] args) {
        System.out.println("Ex9");
        Scanner in = new Scanner(System.in);

        System.out.println("Ex. 9: ");
        Product pufuletiCristinel = new Product();
        Product pufuletiCristinuta = new Product();
        pufuletiCristinel.setCreatedBy("SRL \"PufuletiMD\"");
        pufuletiCristinel.setLocation("Central Repository");
        pufuletiCristinel.setProductName("Cristinel");
        pufuletiCristinel.setTypeOfProduct("Pufuleti din porumb");

        pufuletiCristinuta.setCreatedBy("SRL \"PufuletiMD\"");
        pufuletiCristinuta.setLocation("Central Repository");
        pufuletiCristinuta.setProductName("Cristinuta");
        pufuletiCristinuta.setTypeOfProduct("Pufuleti din porumb");

        TransportCompany truck = new TransportCompany();
        truck.setCarIndex(123);
        truck.setTrunk("empty");

        truck.transportation(pufuletiCristinel.getLocation(),"Chisinau Fidesco",pufuletiCristinel.getProductName());
        truck.transportation(pufuletiCristinel.getLocation(),"Chisinau Eldorado",pufuletiCristinel.getProductName());
        truck.transportation(pufuletiCristinel.getLocation(),"Chisinau Metro",pufuletiCristinel.getProductName());


    }
}
